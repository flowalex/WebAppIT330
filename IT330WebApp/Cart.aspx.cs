﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IT330WebApp
{
    public partial class Cart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ArrayList prods = new ArrayList();
            ArrayList qtys = new ArrayList();

            prods = (ArrayList)Session["cartprod"];
            qtys = (ArrayList)Session["cartqty"];

            if (prods.Count > 0)
            {
                // ==================== 

                SqlDataSource ds = new SqlDataSource();

                string sqlstatement = "SELECT ROW_NUMBER() over(order by ProductID) as ROWNUM, ProductID, Brand, Model, Price, 0 as QTY_ORDER, 0 as LineTotal  FROM [Products] ";

                if (prods.Count > 1)
                {
                    sqlstatement = sqlstatement + " WHERE ProductID = " + prods[0].ToString();
                    for (int i = 1; i < prods.Count; i++)
                    {
                        sqlstatement = sqlstatement + " OR ProductID = " + prods[i].ToString();
                    }
                }
                else
                {
                    sqlstatement = sqlstatement + "WHERE ProductID = " + prods[0].ToString();
                }
                ds.SelectCommand = sqlstatement;

                ds.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["IT330ConnectionString"].ConnectionString;

                ds.DataBind();

                GridView1.DataSource = ds;
                GridView1.DataBind();


                // Populate Quantities
                for (int i = 0; i < qtys.Count; i++)
                {
                    GridView1.Rows[i].Cells[6].Text = qtys[i].ToString();
                }

                decimal autoprice;
                int qtypur;
                decimal linetotresult = 0;
                decimal basket_total = 0;
                int qty_total = 0;

                // Populate Line Totals and Accumulate Basket Total
                for (int i = 1; i <= prods.Count; i++)
                {
                    autoprice = Decimal.Parse(GridView1.Rows[i - 1].Cells[5].Text);
                    qtypur = Int16.Parse(GridView1.Rows[i - 1].Cells[6].Text);
                    linetotresult = autoprice * qtypur;
                    qty_total = (int)qty_total + qtypur;
                    basket_total = basket_total + linetotresult;
                    GridView1.Rows[i - 1].Cells[7].Text = linetotresult.ToString("C", CultureInfo.CurrentCulture);
                }

                // Provide Summary
                GridView1.FooterRow.Cells[5].Text = "Totals";
                GridView1.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                GridView1.FooterRow.Cells[6].Text = qty_total.ToString();
                GridView1.FooterRow.Cells[7].Text = basket_total.ToString("C", CultureInfo.CurrentCulture);
            }
        }


        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ArrayList prods = new ArrayList();
            ArrayList qty = new ArrayList();

            prods = (ArrayList)Session["cartprod"];
            qty = (ArrayList)Session["cartqty"];

            prods.RemoveAt(e.RowIndex);
            qty.RemoveAt(e.RowIndex);

            Session["cartprod"] = (ArrayList)prods;
            Session["cartqty"] = (ArrayList)qty;

            Response.Redirect("cart.aspx");
        }


        protected void btn_checkout_Click(object sender, EventArgs e)
        {
            ArrayList prods = new ArrayList();
            ArrayList qty = new ArrayList();
            prods = (ArrayList)Session["cartprod"];
            qty = (ArrayList)Session["cartqty"];

            string custid = Context.User.Identity.Name;

            if (custid != "")
            {
                DateTime currdatetime = DateTime.Now;
                string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["IT330ConnectionString"].ConnectionString;
                SqlConnection conn = new SqlConnection(connStr);

                string insert_cmd = "insert into sales(custid, prodid, saleprice, saledate, saleqty) values(@custid, @prodid, @saleprice, @saledate, @saleqty)";
                try
                {
                    conn.Open();
                    for (int i = 0; i <= prods.Count; i++)
                    {
                        SqlCommand icmd = new SqlCommand(insert_cmd, conn);
                        icmd.Parameters.AddWithValue("@custid", custid);
                        icmd.Parameters.AddWithValue("@prodid", prods[i]);
                        icmd.Parameters.AddWithValue("@saleprice", Decimal.Parse(GridView1.Rows[i].Cells[5].Text));
                        icmd.Parameters.AddWithValue("@saledate", currdatetime);
                        icmd.Parameters.AddWithValue("@saleqty", qty[i]);
                        icmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('DB Save Problem');", true);
                }
                finally
                { conn.Close(); }

                Session["cartprod"] = new ArrayList();
                Session["cartqty"] = new ArrayList();

                Response.Redirect("confirm.aspx");
            }

            else
            { Response.Redirect("Account/Login"); }
        }
    }
}
