﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="IT330WebApp.ProductDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:IT330ConnectionString %>" SelectCommand="SELECT [ProductID], [Brand], [Model], [Price], [ProductImage], [OtherData], [ProductCategory] FROM [Products] WHERE ([ProductID] = @ProductID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <h2>Product Details</h2><br />
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource1" Height="50px" OnPageIndexChanging="DetailsView1_PageIndexChanging" Width="125px" OnItemCommand="DetailsView1_ItemCommand">
        <Fields>
            <asp:BoundField DataField="ProductID" HeaderText="ProductID" ReadOnly="True" SortExpression="ProductID" />
            <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" />
            <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:BoundField DataField="ProductImage" HeaderText="ProductImage" SortExpression="ProductImage" />
            <asp:BoundField DataField="OtherData" HeaderText="OtherData" SortExpression="OtherData" />
            <asp:BoundField DataField="ProductCategory" HeaderText="ProductCategory" SortExpression="ProductCategory" />
            <asp:ButtonField CommandName="AddToCart" Text="Add To Cart" />
            <asp:ButtonField CommandName="SubmitReview" Text="Submit Review" />
        </Fields>
    </asp:DetailsView>
 Quantity Desired:
        <asp:TextBox ID="QTY_ORDER" runat="server"></asp:TextBox>
    <br />


    <p>
        <strong>Here are some reviews submitted by the customers:</strong></p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Height="197px" ShowHeader="False" Width="778px" AllowPaging="True" AllowSorting="True" DataKeyNames="review_id">
            <Columns>
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID">
                </asp:BoundField>
                <asp:BoundField DataField="date" HeaderText="date" SortExpression="date">
                </asp:BoundField>
                <asp:BoundField DataField="Rating" HeaderText="Rating" SortExpression="Rating">
                </asp:BoundField>
                <asp:BoundField DataField="Review" HeaderText="Review" SortExpression="Review" />
                <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
                <asp:BoundField DataField="review_id" HeaderText="review_id" ReadOnly="True" SortExpression="review_id" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:IT330ConnectionString %>" SelectCommand="SELECT [ProductID], [date], [Rating], [Review], [Username], [review id] AS review_id FROM [Reviews] WHERE ([ProductID] = @ProductID)">
            <SelectParameters>
                <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID=" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
</p>
    <p>
</p>
</asp:Content>
