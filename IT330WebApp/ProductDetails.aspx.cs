﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IT330WebApp
{
    public partial class ProductDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DetailsView1_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {

        }

        protected void DetailsView1_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName == "AddToCart")
            {

                DetailsViewRow row = DetailsView1.Rows[0];
                string prodid = row.Cells[1].Text;

                ArrayList prods = new ArrayList();
                ArrayList qtys = new ArrayList();

                prods = (ArrayList)Session["cartprod"];
                qtys = (ArrayList)Session["cartqty"];

                prods.Add(prodid);
                qtys.Add(QTY_ORDER.Text);

                Session["cartprod"] = prods;
                Session["cartqty"] = qtys;

                Response.Redirect("cart.aspx");
            }

            else if (e.CommandName == "SubmitReview")

            {

                string url = "newreview.aspx?";

                DetailsViewRow row = DetailsView1.Rows[0];
                string autoid = row.Cells[1].Text;

                url += "autoid=" + autoid;
                Response.Redirect(url);
            }
        }
    }
}