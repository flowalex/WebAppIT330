﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IT330WebApp
{
    public partial class Products : System.Web.UI.Page
    {
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridView1.SelectedRow;
            string d = row.Cells[1].Text;

            string url = "ProductDetails.aspx?";
            url += "ProductID=" + d;
            Response.Redirect(url);
        }
    }
}