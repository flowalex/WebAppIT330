﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IT330WebApp.Startup))]
namespace IT330WebApp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
