﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="newreview.aspx.cs" Inherits="IT330WebApp.newreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Username: "></asp:Label>
   <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:IT330ConnectionString %>" DeleteCommand="DELETE FROM [Review] WHERE [username] = @username AND [submitdate] = @submitdate AND [autoid] = @autoid" InsertCommand="INSERT INTO [Review] ([username], [comments], [rating], [submitdate], [autoid]) VALUES (@username, @comments, @rating, @submitdate, @autoid)" SelectCommand="SELECT * FROM [Review]" UpdateCommand="UPDATE [Review] SET [comments] = @comments, [rating] = @rating WHERE [username] = @username AND [submitdate] = @submitdate AND [autoid] = @autoid">
        <DeleteParameters>
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="submitdate" Type="DateTime" />
            <asp:Parameter Name="ProductID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="comments" Type="String" />
            <asp:Parameter Name="rating" Type="Int32" />
            <asp:Parameter Name="submitdate" Type="DateTime" />
            <asp:Parameter Name="ProductID" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="comments" Type="String" />
            <asp:Parameter Name="rating" Type="Int32" />
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="submitdate" Type="DateTime" />
            <asp:Parameter Name="ProductID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="username,submitdate,ProductID" DataSourceID="SqlDataSource1" DefaultMode="Insert" Height="231px" OnItemInserted="FormView1_ItemInserted" Width="545px">
        <EditItemTemplate>
            username:
            <asp:Label ID="usernameLabel1" runat="server" Text='<%# Eval("username") %>' />
            <br />
            comments:
            <asp:TextBox ID="commentsTextBox" runat="server" Text='<%# Bind("comments") %>' />
            <br />
            rating:
            <asp:TextBox ID="ratingTextBox" runat="server" Text='<%# Bind("rating") %>' />
            <br />
            submitdate:
            <asp:Label ID="submitdateLabel1" runat="server" Text='<%# Eval("submitdate") %>' />
            <br />
            ProductID:
            <asp:Label ID="autoidLabel1" runat="server" Text='<%# Eval("ProductID") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
            username:
            <asp:TextBox ID="usernameTextBox" runat="server" Text='<%# Bind("username") %>' />
            <br />
            comments:
            <asp:TextBox ID="commentsTextBox" runat="server" Text='<%# Bind("comments") %>' />
            <br />
            rating:
            <asp:TextBox ID="ratingTextBox" runat="server" Text='<%# Bind("rating") %>' />
            <br />
            submitdate:
            <asp:TextBox ID="submitdateTextBox" runat="server" Text='<%# Bind("submitdate") %>' />
            <br />
            ProductID:
            <asp:TextBox ID="autoidTextBox" runat="server" Text='<%# Bind("ProductID") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            username:
            <asp:Label ID="usernameLabel" runat="server" Text='<%# Eval("username") %>' />
            <br />
            comments:
            <asp:Label ID="commentsLabel" runat="server" Text='<%# Bind("comments") %>' />
            <br />
            rating:
            <asp:Label ID="ratingLabel" runat="server" Text='<%# Bind("rating") %>' />
            <br />
            submitdate:
            <asp:Label ID="submitdateLabel" runat="server" Text='<%# Eval("submitdate") %>' />
            <br />
            ProductID:
            <asp:Label ID="autoidLabel" runat="server" Text='<%# Eval("ProductID") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
        </ItemTemplate>
    </asp:FormView>
</asp:Content>
