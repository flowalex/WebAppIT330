﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IT330WebApp
{
    public partial class newreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ProductID = Request.QueryString["ProductID"];

            TextBox auto = (TextBox)FormView1.FindControl("autoidTextBox");

            auto.Text = ProductID;

            TextBox username = (TextBox)FormView1.FindControl("usernameTextBox");
            username.Text = Context.User.Identity.Name;
        }



        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            Response.Redirect("ProductDetails.aspx?ProductID=" + Request.QueryString["ProductID"]);
        }

    }
}